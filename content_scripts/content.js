function checkNumber() {
    let selectorTask = '.task-row';
    let selectorIndicator = '.h-indicator-icon--board-column';
    let selectorRowContentHolder = '.w-task-row__name';

    let elements = document.querySelectorAll(selectorTask);
    if (!elements) {
        return;
    }

    for (let elemTr of elements) {
        let rowContentHolder = elemTr.querySelector(selectorRowContentHolder);
        if (!rowContentHolder) {
            continue;
        }

        let indicator = elemTr.querySelector(selectorIndicator);
        if (indicator) {
            let color = indicator.style.color;
            rowContentHolder.style.color = color.replace('color: ', '');
        } else {
            rowContentHolder.style.color = 'rgb(11, 14, 31)';
        }
    }
}

window.onload = function () {
    setInterval(checkNumber, 1000);
}();